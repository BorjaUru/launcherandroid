package com.bulsa.bulsalauncher.activitys;

import android.app.Activity;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.bulsa.bulsalauncher.App;
import com.bulsa.bulsalauncher.R;
import com.bulsa.bulsalauncher.Util;

import java.util.ArrayList;

public class MainActivity extends Activity{
    private ImageView iv;

    private TableLayout tb2;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String a= getIntent().getStringExtra("p");
        if(a!=null) {
            Toast.makeText(this,a,Toast.LENGTH_LONG).show();
            recogerIntent(a);
        }
        iv=(ImageView) findViewById(R.id.ivapps);
        iv.setImageResource(R.drawable.a);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boton();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void boton() {
        Intent i =new Intent(this, AppsListActivity.class);
        startActivity(i);
        finish();
    }
    private void recogerIntent(String a){
        tb2=(TableLayout)findViewById(R.id.tb2);
            PackageManager m=getPackageManager();
            ArrayList<App>aa= Util.arrayApps(m);

            for(App aaa:aa){

                if(aaa.paquete.toString().equals(a)){
                    TableRow tbr=new TableRow(this);
                    ImageView iv=new ImageView(this);
                    iv.setImageDrawable(aaa.icono);

                    tb2.addView(tbr);
                    tbr.addView(iv);

                }
            }



    }
}
