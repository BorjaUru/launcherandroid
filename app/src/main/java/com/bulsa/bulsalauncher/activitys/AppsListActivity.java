package com.bulsa.bulsalauncher.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.bulsa.bulsalauncher.App;
import com.bulsa.bulsalauncher.R;
import com.bulsa.bulsalauncher.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AppsListActivity extends Activity {
    private PackageManager m;
    private List<App>apps1;
    private TableLayout tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_list);
        cargarApps();
        tabla();
    }

    private void tabla() {
       int count=0;
        tb=(TableLayout)findViewById(R.id.tb);
        for(int i=0;i<apps1.size()/3;i++){
            TableRow tbr=new TableRow(this);
            tb.addView(tbr);
            for(int e=0;e<3;e++){
                if(count<apps1.size()){
                    ImageView iv=new ImageView(this);
                    App app=apps1.get(count);
                    iv.setTag(app.paquete);
                    iv.setImageDrawable(app.icono);
                    iv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ImageView a=(ImageView)view;
                            Intent ii=m.getLaunchIntentForPackage(a.getTag().toString());
                            AppsListActivity.this.startActivity(ii);
                        }
                    });
                    iv.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            ImageView a=(ImageView)view;
                            a(a.getTag().toString());
                            return true;
                        }
                    });
                    tbr.addView(iv);

                }
                count++;

            }
            TableRow tbr1=new TableRow(this);
            tb.addView(tbr1);
            TextView tv1=new TextView(this);
            tv1.setText(apps1.get(count-3).nombre);
            tv1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            tbr1.addView(tv1);
            TextView tv2=new TextView(this);
            tv2.setText(apps1.get(count-2).nombre);
            tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            tbr1.addView(tv2);
            TextView tv3=new TextView(this);
            tv3.setText(apps1.get(count-1).nombre);
            tv3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            tbr1.addView(tv3);

        }

    }


    public void a(String a){

        for(App aa:apps1){
            if(aa.paquete.toString().equals(a)){
                Intent i =new Intent(this, MainActivity.class);


                i.putExtra("p",a);

                startActivity(i);

            }
        }

    }
    private void cargarApps() {

        m= getPackageManager();
        apps1= Util.arrayApps(m);
    }
}
