package com.bulsa.bulsalauncher;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by borja on 27/06/2017.
 */

public class Util {

    public static ArrayList<App> arrayApps(PackageManager m){
        ArrayList apps1=new ArrayList();
        Intent i=new Intent(Intent.ACTION_MAIN,null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> aa=m.queryIntentActivities(i,0);
        for(ResolveInfo r:aa){
            App a=new App();
            a.nombre=r.loadLabel(m);
            a.icono=r.activityInfo.loadIcon(m);
            a.paquete=r.activityInfo.packageName;
            apps1.add(a);
        }
        Collections.sort(apps1, new Comparator<App>() {
            @Override
            public int compare(App app, App t1) {
                return app.nombre.toString().compareTo(t1.nombre.toString());
            }
        });
        return apps1;
    }
}
